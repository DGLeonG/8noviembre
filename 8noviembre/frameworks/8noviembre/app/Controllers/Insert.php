<?php

namespace App\Controllers;
use App\Models\ProModel;

class Insert extends BaseController
{
    public function insertProducto(){
        $data['title'] = "Insertar Producto";
        return view('producto/formInsertPro',$data);
    }

    public function productoInsert() {
        
        $proModel = new ProModel();
        
        $codigoproducto = $this->request->getPost("codigoproducto");
        $nombre = $this->request->getPost("nombre");
        $codigofamilia = $this->request->getPost("codigofamilia");
        $caracteristicas = $this->request->getPost("caracteristicas");
        $color = $this->request->getPost('color');
        $nif = $this->request->getPost("tipoiva");
        $producto=["codigoproducto"=>$codigoproducto,
                "nombre"=>$nombre,
                "codigofamilia"=>$codigofamilia,
                "caracteristicas"=>$caracteristicas,
                "color"=>$color,
                "tipoiva"=>$tipoiva];
        $proModel->insert($producto);
    }
}