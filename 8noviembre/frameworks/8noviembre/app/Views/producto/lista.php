<?php

?>

<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <title><?=$title?></title>
    </head>
    <body>
         <h1 class="text-primary"><?= $title?></h1>
        <table class="table table-striped">
            <?php foreach ($result as $producto): ?>
                <tr>
                    <td>
                        <?= $producto->CodigoProducto ?>
                    </td>
                    <td>
                        <?= $producto->Nombre ?>
                    </td>
                    <td>
                        <?= $producto->CodigoFamilia ?>
                    </td>
                    <td>
                        <?= $producto->Caracteristicas ?>
                    </td>
                    <td>
                        <?= $producto->Color ?>
                    </td>
                    <td>
                        <?= $producto->TipoIVA ?>
                    </td>
                    <td>
                        <?= $producto->NombreFamilia ?>
                    </td>
                    <td>
                        <button>Borrar</button> 
                    </td>
                </tr>

            <?php endforeach; ?>
        </table>
    </body>
</html>

