<?php

?> 

<html>
    <center>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <title><?= $title?></title>
    </head>
    <body>
        <h1 class="text-primary"><?= $title?></h1>
        <form action="<?= site_url('productoInsert')?>" method="Post">
                <label for="codigoproducto" class="text-success">Codigo Producto: </label>
                <input type="text" name="codigoproducto" placeholder="CodigoProducto" id="codigoproducto" />
                
                <br><br>
                
                <label for="nombre" class="text-success">Nombre: </label>
                <input type="text" name="nombre" placeholder="Nombre" id="nombre" />
                
                <br><br>
                               
                <label for="codigofamilia" class="text-success">Código familia: </label>
                <input type="text" name="codigofamilia" placeholder="Código familia" id="codigofamilia" />
                
                <br><br>
                
                <label for="caracteristicas" class="text-success">Características: </label>
                <input type="text" name="caracteristicas" placeholder="Características" id="caracteristicas" />
                
                <br><br>               
                
                <label for="color" class="text-success">Color: </label>
                <input type="text" name="Color" id="color" />
                
                <br><br>
                
                <label for="tipoiva" class="text-success">Tipo Iva: </label>
                <input type="text" name="Tipo Iva" id="tipoiva" />
                
                <br><br>  
                
            <input type="submit" name="enviar" value="Enviar" />
        </form>
    </body>
    </center>
</html>
